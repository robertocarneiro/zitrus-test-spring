FROM adoptopenjdk/openjdk11:alpine-jre

COPY ./target/*.jar /app.jar

ENV JAR_OPTIONS "-Duser.country=BR -Duser.language=pt -Dfile.encoding=UTF8 -Duser.timezone=America/Sao_Paulo -Dspring.profiles.active=docker"

EXPOSE 8090/tcp
CMD java $JAR_OPTIONS -jar /app.jar