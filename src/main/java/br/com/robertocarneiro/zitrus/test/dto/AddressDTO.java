package br.com.robertocarneiro.zitrus.test.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class AddressDTO {

    @NotEmpty
    private String postalCode;
    @NotEmpty
    private String street;
    @NotEmpty
    private String number;
    private String complement;
    @NotEmpty
    private String district;
    @NotEmpty
    private String city;
    @NotEmpty
    private String state;
}
