package br.com.robertocarneiro.zitrus.test.controller;

import br.com.robertocarneiro.zitrus.test.dto.CepAddressDTO;
import br.com.robertocarneiro.zitrus.test.service.AddressService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/addresses")
@RequiredArgsConstructor
@SecurityRequirement(name = "basicAuth")
public class AddressController {

    private final AddressService service;

    @GetMapping("/{cep}/open-cep")
    public CepAddressDTO findOpenCepByCep(@PathVariable String cep) {
        return service.findOpenCepByCep(cep);
    }

}
