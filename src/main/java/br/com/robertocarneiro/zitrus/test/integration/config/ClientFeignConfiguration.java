package br.com.robertocarneiro.zitrus.test.integration.config;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@Configuration
public class ClientFeignConfiguration {

    @Bean
    public ErrorDecoder errorDecoder() {
        return (s, response) -> new ResponseStatusException(HttpStatus.valueOf(response.status()), response.reason());
    }

}
