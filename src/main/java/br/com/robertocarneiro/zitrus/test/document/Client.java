package br.com.robertocarneiro.zitrus.test.document;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Document
public class Client {

    @Id
    private String cpf;
    private String fullName;
    private LocalDate birthDate;
    private List<Address> addresses;
}
