package br.com.robertocarneiro.zitrus.test.mapper;

import br.com.robertocarneiro.zitrus.test.dto.CepAddressDTO;
import br.com.robertocarneiro.zitrus.test.integration.dto.OpenCepAddressDTO;
import br.com.robertocarneiro.zitrus.test.util.AddressUtil;
import org.mapstruct.Mapper;

@Mapper(imports = AddressUtil.class,
        componentModel = "spring")
public interface CepAddressMapper {

    CepAddressDTO openCepAddressToCepAddress(OpenCepAddressDTO openCepAddressDTO);
}
