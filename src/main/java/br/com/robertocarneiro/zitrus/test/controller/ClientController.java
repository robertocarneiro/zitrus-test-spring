package br.com.robertocarneiro.zitrus.test.controller;

import br.com.robertocarneiro.zitrus.test.dto.ClientDTO;
import br.com.robertocarneiro.zitrus.test.service.ClientService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/clients")
@RequiredArgsConstructor
@SecurityRequirement(name = "basicAuth")
public class ClientController {

    private final ClientService service;

    @GetMapping
    public Page<ClientDTO> findAll(@RequestParam(defaultValue = "0") Integer page,
                                   @RequestParam(defaultValue = "10") Integer size) {

        final Sort sort = Sort.by(Sort.Direction.ASC, "fullName");
        final Pageable pageable = PageRequest.of(page, size, sort);

        return service.findAll(pageable);
    }

    @GetMapping("/{cpf}")
    public ClientDTO findByCpf(@PathVariable String cpf) {
        return service.findByCpf(cpf);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDTO create(@Valid @RequestBody ClientDTO clientDTO) {
        return service.create(clientDTO);
    }

    @PutMapping("/{cpf}")
    public ClientDTO edit(@PathVariable String cpf,
                          @Valid @RequestBody ClientDTO clientDTO) {
        return service.edit(cpf, clientDTO);
    }

    @DeleteMapping("/{cpf}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String cpf) {
        service.delete(cpf);
    }

}
