package br.com.robertocarneiro.zitrus.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ZitrusTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZitrusTestApplication.class, args);
    }

}
