package br.com.robertocarneiro.zitrus.test.document;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Document
public class Address {

    private String postalCode;
    private String street;
    private String number;
    private String complement;
    private String district;
    private String city;
    private String state;
    private String fullAddress;
}
