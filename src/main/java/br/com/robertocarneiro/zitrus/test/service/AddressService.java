package br.com.robertocarneiro.zitrus.test.service;

import br.com.robertocarneiro.zitrus.test.dto.AddressDTO;
import br.com.robertocarneiro.zitrus.test.dto.CepAddressDTO;
import br.com.robertocarneiro.zitrus.test.integration.client.OpenCepClient;
import br.com.robertocarneiro.zitrus.test.integration.dto.OpenCepAddressDTO;
import br.com.robertocarneiro.zitrus.test.mapper.CepAddressMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final OpenCepClient openCepClient;
    private final CepAddressMapper cepAddressMapper;

    public CepAddressDTO findOpenCepByCep(String cep) {
        OpenCepAddressDTO openCepAddressDTO = openCepClient.findByCep(cep);
        openCepAddressDTO.setPostalCode(openCepAddressDTO.getPostalCode().replace("-", ""));
        return cepAddressMapper.openCepAddressToCepAddress(openCepAddressDTO);
    }

    public boolean areAddressesValid(List<AddressDTO> addresses) {
        if (isNull(addresses)) {
            return true;
        }
        return addresses.stream().allMatch(this::isAddressValid);
    }

    private boolean isAddressValid(AddressDTO addressDTO) {
        if (isNull(addressDTO) || isNull(addressDTO.getPostalCode())) {
            return false;
        }

        CepAddressDTO cepAddressDTO = findOpenCepByCep(addressDTO.getPostalCode());

        return Objects.equals(addressDTO.getPostalCode(), cepAddressDTO.getPostalCode())
                && Objects.equals(addressDTO.getStreet(), cepAddressDTO.getStreet())
                && Objects.equals(addressDTO.getDistrict(), cepAddressDTO.getDistrict())
                && Objects.equals(addressDTO.getCity(), cepAddressDTO.getCity())
                && Objects.equals(addressDTO.getState(), cepAddressDTO.getState());
    }
}
