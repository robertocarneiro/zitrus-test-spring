package br.com.robertocarneiro.zitrus.test.config;

import br.com.robertocarneiro.zitrus.test.dto.ErrorResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.List;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class BasicWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private final ObjectMapper objectMapper;

    private static final String[] antPatterns = new String[]{
            "/v3/api-docs",
            "/v3/api-docs/*",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui/*"
    };

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests(auth -> auth
                        .antMatchers(HttpMethod.GET, antPatterns).permitAll()
                        .anyRequest().authenticated())
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().httpBasic()
                .and().cors()
                .and().csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return (request, response, ex) -> {
            String title = "User unauthorized";
            HttpStatus status = HttpStatus.UNAUTHORIZED;

            response.setStatus(status.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);

            ErrorResponseDTO errorResponseDTO = ErrorResponseDTO
                    .builder()
                    .messages(List.of(title))
                    .dateTime(LocalDateTime.now())
                    .build();

            OutputStream out = response.getOutputStream();
            objectMapper.writeValue(out, errorResponseDTO);
            out.flush();
        };
    }
}
