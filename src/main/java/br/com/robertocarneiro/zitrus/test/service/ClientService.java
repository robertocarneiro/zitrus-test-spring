package br.com.robertocarneiro.zitrus.test.service;

import br.com.robertocarneiro.zitrus.test.document.Client;
import br.com.robertocarneiro.zitrus.test.dto.ClientDTO;
import br.com.robertocarneiro.zitrus.test.mapper.ClientMapper;
import br.com.robertocarneiro.zitrus.test.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository repository;
    private final ClientMapper mapper;
    private final AddressService addressService;

    public Page<ClientDTO> findAll(Pageable pageable) {
        return repository
                .findAll(pageable)
                .map(mapper::documentToDTO);
    }

    public ClientDTO findByCpf(String cpf) {
        return repository
                .findById(cpf)
                .map(mapper::documentToDTO)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cpf not found"));
    }

    public ClientDTO create(ClientDTO clientDTO) {
        clientDTO.setCpf(clearCPF(clientDTO.getCpf()));
        if (repository.findById(clientDTO.getCpf()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cliente já cadastrado, acesse a listagem e edite o cliente");
        }
        return save(clientDTO);
    }

    public ClientDTO edit(String cpf, ClientDTO clientDTO) {
        if (!Objects.equals(cpf, clientDTO.getCpf())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict cpf");
        }
        clientDTO.setCpf(clearCPF(clientDTO.getCpf()));
        findByCpf(clientDTO.getCpf());
        return save(clientDTO);
    }

    private String clearCPF(String cpf) {
        return cpf
                .replace(".", "")
                .replace("-", "");
    }

    private ClientDTO save(ClientDTO clientDTO) {
        if (!addressService.areAddressesValid(clientDTO.getAddresses())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Addresses are invalid");
        }
        Client client = mapper.dtoToDocument(clientDTO);
        client = repository.save(client);
        return mapper.documentToDTO(client);
    }

    public void delete(String cpf) {
        ClientDTO clientDTO = findByCpf(cpf);
        repository.deleteById(clientDTO.getCpf());
    }
}
