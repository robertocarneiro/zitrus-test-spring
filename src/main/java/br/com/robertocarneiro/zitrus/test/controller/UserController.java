package br.com.robertocarneiro.zitrus.test.controller;

import br.com.robertocarneiro.zitrus.test.dto.UserDTO;
import br.com.robertocarneiro.zitrus.test.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@SecurityRequirement(name = "basicAuth")
public class UserController {

    private final UserService service;

    @GetMapping("/authenticate")
    public UserDTO authenticate(@RequestHeader("Authorization") String authorization) {
        return service.authenticate(authorization);
    }

}
