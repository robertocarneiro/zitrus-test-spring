package br.com.robertocarneiro.zitrus.test.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class OpenCepAddressDTO {

    @JsonProperty("cep")
    private String postalCode;
    @JsonProperty("logradouro")
    private String street;
    @JsonProperty("complemento")
    private String complement;
    @JsonProperty("bairro")
    private String district;
    @JsonProperty("localidade")
    private String city;
    @JsonProperty("uf")
    private String state;
    private String ibge;
}
