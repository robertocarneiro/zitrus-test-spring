package br.com.robertocarneiro.zitrus.test.repository;

import br.com.robertocarneiro.zitrus.test.document.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientRepository extends MongoRepository<Client, String> {
}
