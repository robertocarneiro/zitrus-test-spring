package br.com.robertocarneiro.zitrus.test.mapper;

import br.com.robertocarneiro.zitrus.test.document.Client;
import br.com.robertocarneiro.zitrus.test.dto.ClientDTO;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;

@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
        uses = AddressMapper.class,
        componentModel = "spring")
public interface ClientMapper {

    ClientDTO documentToDTO(Client client);
    Client dtoToDocument(ClientDTO clientDTO);
}
