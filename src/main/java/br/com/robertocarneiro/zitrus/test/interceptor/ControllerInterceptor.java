package br.com.robertocarneiro.zitrus.test.interceptor;

import br.com.robertocarneiro.zitrus.test.dto.ErrorResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@RestControllerAdvice
@RequiredArgsConstructor
public class ControllerInterceptor extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorResponseDTO> handleResponseStatusException(ResponseStatusException ex) {
        List<String> messages = isNull(ex.getReason()) ? List.of() : List.of(ex.getReason());
        ErrorResponseDTO errorResponseDTO = ErrorResponseDTO
                .builder()
                .messages(messages)
                .dateTime(LocalDateTime.now())
                .build();
        return ResponseEntity.status(ex.getStatus()).body(errorResponseDTO);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponseDTO> handleResponseStatusException(Exception ex) {
        ErrorResponseDTO errorResponseDTO = ErrorResponseDTO
                .builder()
                .messages(List.of(ex.getMessage()))
                .dateTime(LocalDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponseDTO);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> messages = ex.getBindingResult().getFieldErrors().stream().map(this::buildMessage).collect(Collectors.toList());
        ErrorResponseDTO error = ErrorResponseDTO
                .builder()
                .messages(messages)
                .dateTime(LocalDateTime.now())
                .build();

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private String buildMessage(FieldError fieldError) {
        return fieldError.getField() + ": " + fieldError.getDefaultMessage();
    }
}
