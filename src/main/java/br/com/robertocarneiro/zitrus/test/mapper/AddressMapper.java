package br.com.robertocarneiro.zitrus.test.mapper;

import br.com.robertocarneiro.zitrus.test.document.Address;
import br.com.robertocarneiro.zitrus.test.dto.AddressDTO;
import br.com.robertocarneiro.zitrus.test.util.AddressUtil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(imports = AddressUtil.class,
        componentModel = "spring")
public interface AddressMapper {

    AddressDTO documentToDTO(Address address);
    @Mapping(target = "fullAddress", expression = "java(AddressUtil.buildFullAddress(addressDTO))")
    Address dtoToDocument(AddressDTO addressDTO);
}
