package br.com.robertocarneiro.zitrus.test.util;

import br.com.robertocarneiro.zitrus.test.dto.AddressDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AddressUtil {

    public static final String COMMA = ", ";

    public static String buildFullAddress(AddressDTO addressDTO) {
        StringBuilder stringBuilder = new StringBuilder();
        if (isNull(addressDTO)) {
            return stringBuilder.toString();
        }
        stringBuilder
                .append(addressDTO.getStreet())
                .append(COMMA)
                .append(addressDTO.getNumber())
                .append(COMMA);
        if (nonNull(addressDTO.getComplement())) {
            stringBuilder.append(addressDTO.getComplement()).append(COMMA);
        }
        return stringBuilder
                .append(addressDTO.getDistrict())
                .append(COMMA)
                .append(addressDTO.getCity())
                .append(COMMA)
                .append(addressDTO.getState())
                .append(" ")
                .append(addressDTO.getPostalCode())
                .toString();
    }
}
