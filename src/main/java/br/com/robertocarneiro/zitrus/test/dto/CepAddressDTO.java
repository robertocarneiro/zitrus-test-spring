package br.com.robertocarneiro.zitrus.test.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CepAddressDTO {

    private String postalCode;
    private String street;
    private String complement;
    private String district;
    private String city;
    private String state;
}
