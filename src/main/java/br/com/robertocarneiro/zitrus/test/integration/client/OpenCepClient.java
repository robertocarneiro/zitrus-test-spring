package br.com.robertocarneiro.zitrus.test.integration.client;

import br.com.robertocarneiro.zitrus.test.integration.config.ClientFeignConfiguration;
import br.com.robertocarneiro.zitrus.test.integration.dto.OpenCepAddressDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        value = "openCepClient",
        url = "${integration.open-cep.path}",
        configuration = ClientFeignConfiguration.class)
public interface OpenCepClient {

    @GetMapping("/{cep}")
    OpenCepAddressDTO findByCep(@PathVariable String cep);
}
