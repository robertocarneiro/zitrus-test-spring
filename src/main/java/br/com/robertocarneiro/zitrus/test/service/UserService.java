package br.com.robertocarneiro.zitrus.test.service;

import br.com.robertocarneiro.zitrus.test.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    public UserDTO authenticate(String authorization) {
        return UserDTO.builder().authdata(authorization).build();
    }
}
