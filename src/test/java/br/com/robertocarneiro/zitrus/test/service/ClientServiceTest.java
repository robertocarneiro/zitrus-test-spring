package br.com.robertocarneiro.zitrus.test.service;

import br.com.robertocarneiro.zitrus.test.document.Client;
import br.com.robertocarneiro.zitrus.test.dto.ClientDTO;
import br.com.robertocarneiro.zitrus.test.mapper.ClientMapper;
import br.com.robertocarneiro.zitrus.test.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @InjectMocks
    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ClientMapper clientMapper;

    @Mock
    private AddressService addressService;

    private static final String CPF = "12345678901";

    @Test
    void findAllPageable_whenRepositoryReturnException_thenReturnException() {

        Pageable pageable = Pageable.unpaged();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findAll(pageable)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.findAll(pageable));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findAll(pageable);
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
    }

    @Test
    void findAllPageable_whenMapperReturnException_thenReturnException() {

        Pageable pageable = Pageable.ofSize(10);
        Client client = Client.builder().build();
        List<Client> clients = List.of(client);
        Page<Client> clientPage = new PageImpl<>(clients, pageable, 1);
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findAll(pageable)).thenReturn(clientPage);
        when(clientMapper.documentToDTO(client)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.findAll(pageable));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findAll(pageable);
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
    }

    @Test
    void findAllPageable_whenMapperReturnSuccess_thenReturnSuccess() {

        Pageable pageable = Pageable.ofSize(10);
        Client client = Client.builder().build();
        List<Client> clients = List.of(client);
        Page<Client> clientPage = new PageImpl<>(clients, pageable, 1);
        ClientDTO clientDTO = ClientDTO.builder().build();

        when(clientRepository.findAll(pageable)).thenReturn(clientPage);
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);

        Page<ClientDTO> clientDTOPage = clientService.findAll(pageable);

        assertNotNull(clientDTOPage);
        assertNotNull(clientDTOPage.getContent());
        assertEquals(1, clientDTOPage.getContent().size());
        assertEquals(clientPage.getContent().size(), clientDTOPage.getContent().size());
        assertEquals(clientDTO, clientDTOPage.getContent().get(0));

        verify(clientRepository, times(1)).findAll(pageable);
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
    }

    @Test
    void findByCpf_whenRepositoryReturnException_thenReturnException() {

        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findById(CPF)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.findByCpf(CPF));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findById(CPF);
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
    }

    @Test
    void findByCpf_whenMapperReturnException_thenReturnException() {

        Client client = Client.builder().build();
        Optional<Client> clientOptional = Optional.of(client);
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findById(CPF)).thenReturn(clientOptional);
        when(clientMapper.documentToDTO(client)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.findByCpf(CPF));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findById(CPF);
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
    }

    @Test
    void findByCpf_whenMapperReturnSuccess_thenReturnSuccess() {

        Client client = Client.builder().build();
        Optional<Client> clientOptional = Optional.of(client);
        ClientDTO clientDTO = ClientDTO.builder().build();

        when(clientRepository.findById(CPF)).thenReturn(clientOptional);
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);

        ClientDTO clientDTOResponse = clientService.findByCpf(CPF);

        assertNotNull(clientDTOResponse);
        assertEquals(clientDTO, clientDTOResponse);

        verify(clientRepository, times(1)).findById(CPF);
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
    }

    @Test
    void save_whenClientDTOAlreadyExists_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.CONFLICT, "Cliente já cadastrado, acesse a listagem e edite o cliente");

        when(clientRepository.findById(CPF)).thenReturn(Optional.of(client));

        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> clientService.create(clientDTO));

        assertNotNull(ex);
        assertEquals(responseStatusException.getStatus(), ex.getStatus());
        assertEquals(responseStatusException.getReason(), ex.getReason());

        verify(clientRepository, times(1)).findById(CPF);
        verify(addressService, times(0)).areAddressesValid(null);
        verify(clientMapper, times(0)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(0)).save(any(Client.class));
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
    }

    @Test
    void save_whenAddressServiceAreAddressesValidIsFalse_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Addresses are invalid");

        when(clientRepository.findById(CPF)).thenReturn(Optional.empty());
        when(addressService.areAddressesValid(null)).thenReturn(false);

        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> clientService.create(clientDTO));

        assertNotNull(ex);
        assertEquals(responseStatusException.getStatus(), ex.getStatus());
        assertEquals(responseStatusException.getReason(), ex.getReason());

        verify(clientRepository, times(1)).findById(CPF);
        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientMapper, times(0)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(0)).save(any(Client.class));
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
    }

    @Test
    void save_whenMapperDtoToDocumentReturnException_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findById(CPF)).thenReturn(Optional.empty());
        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientMapper.dtoToDocument(clientDTO)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.create(clientDTO));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findById(CPF);
        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(0)).save(any(Client.class));
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
    }

    @Test
    void save_whenRepositoryReturnException_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findById(CPF)).thenReturn(Optional.empty());
        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientMapper.dtoToDocument(clientDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.create(clientDTO));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findById(CPF);
        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(1)).save(any(Client.class));
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
    }

    @Test
    void save_whenMapperDocumentToDTOReturnException_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findById(CPF)).thenReturn(Optional.empty());
        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientMapper.dtoToDocument(clientDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenReturn(client);
        when(clientMapper.documentToDTO(client)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.create(clientDTO));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findById(CPF);
        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(1)).save(any(Client.class));
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
    }

    @Test
    void save_whenMapperDocumentToDTOReturnSuccess_thenReturnSuccess() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();

        when(clientRepository.findById(CPF)).thenReturn(Optional.empty());
        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientMapper.dtoToDocument(clientDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenReturn(client);
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);

        ClientDTO clientDTOSaved = clientService.create(clientDTO);

        assertNotNull(clientDTOSaved);
        assertEquals(clientDTO, clientDTOSaved);

        verify(clientRepository, times(1)).findById(CPF);
        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(1)).save(any(Client.class));
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
    }

    @Test
    void edit_whenCpfPathVariableAndCpfRequestBody_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().build();

        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> clientService.edit(CPF, clientDTO));

        assertNotNull(ex);
        assertEquals(HttpStatus.CONFLICT, ex.getStatus());

        verify(addressService, times(0)).areAddressesValid(null);
        verify(clientRepository, times(0)).findById(any(String.class));
        verify(clientMapper, times(0)).documentToDTO(any(Client.class));
        verify(clientMapper, times(0)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(0)).save(any(Client.class));
    }

    @Test
    void edit_whenSaveRepositoryReturnException_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientRepository.findById(CPF)).thenReturn(Optional.of(client));
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);
        when(clientMapper.dtoToDocument(clientDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.edit(CPF, clientDTO));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientRepository, times(1)).findById(any(String.class));
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(1)).save(any(Client.class));
    }

    @Test
    void edit_whenMapperDocumentToDTOReturnException_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();
        Client clientReturn = Client.builder().cpf(CPF).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientRepository.findById(CPF)).thenReturn(Optional.of(client));
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);
        when(clientMapper.dtoToDocument(clientDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenReturn(clientReturn);
        when(clientMapper.documentToDTO(clientReturn)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.edit(CPF, clientDTO));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientRepository, times(1)).findById(any(String.class));
        verify(clientMapper, times(2)).documentToDTO(any(Client.class));
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(1)).save(any(Client.class));
    }

    @Test
    void edit_whenMapperDocumentToDTOReturnSuccess_thenReturnSuccess() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();

        when(addressService.areAddressesValid(null)).thenReturn(true);
        when(clientRepository.findById(CPF)).thenReturn(Optional.of(client));
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);
        when(clientMapper.dtoToDocument(clientDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenReturn(client);
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);

        ClientDTO clientDTOSaved = clientService.edit(CPF, clientDTO);

        assertNotNull(clientDTOSaved);
        assertEquals(clientDTO, clientDTOSaved);

        verify(addressService, times(1)).areAddressesValid(null);
        verify(clientRepository, times(1)).findById(any(String.class));
        verify(clientMapper, times(2)).documentToDTO(any(Client.class));
        verify(clientMapper, times(1)).dtoToDocument(any(ClientDTO.class));
        verify(clientRepository, times(1)).save(any(Client.class));
    }

    @Test
    void delete_whenRepositoryDeleteReturnException_thenReturnException() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(clientRepository.findById(CPF)).thenReturn(Optional.of(client));
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);
        doThrow(runtimeException).when(clientRepository).deleteById(CPF);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> clientService.delete(CPF));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(clientRepository, times(1)).findById(any(String.class));
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
        verify(clientRepository, times(1)).deleteById(any(String.class));
    }

    @Test
    void delete_whenRepositoryDeleteReturnSuccess_thenReturnSuccess() {

        ClientDTO clientDTO = ClientDTO.builder().cpf(CPF).build();
        Client client = Client.builder().cpf(CPF).build();

        when(clientRepository.findById(CPF)).thenReturn(Optional.of(client));
        when(clientMapper.documentToDTO(client)).thenReturn(clientDTO);

        clientService.delete(CPF);

        verify(clientRepository, times(1)).findById(any(String.class));
        verify(clientMapper, times(1)).documentToDTO(any(Client.class));
        verify(clientRepository, times(1)).deleteById(any(String.class));
    }
}
