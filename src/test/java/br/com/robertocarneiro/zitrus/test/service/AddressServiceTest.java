package br.com.robertocarneiro.zitrus.test.service;

import br.com.robertocarneiro.zitrus.test.dto.AddressDTO;
import br.com.robertocarneiro.zitrus.test.dto.CepAddressDTO;
import br.com.robertocarneiro.zitrus.test.integration.client.OpenCepClient;
import br.com.robertocarneiro.zitrus.test.integration.dto.OpenCepAddressDTO;
import br.com.robertocarneiro.zitrus.test.mapper.CepAddressMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AddressServiceTest {

    @InjectMocks
    private AddressService addressService;

    @Mock
    private OpenCepClient openCepClient;

    @Mock
    private CepAddressMapper cepAddressMapper;

    private static final String POSTAL_CODE = "12345678";
    private static final String POSTAL_CODE_OPEN_CEP = "12345-678";

    @Test
    void findOpenCepByCep_whenClientReturnException_thenReturnException() {

        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        when(openCepClient.findByCep(POSTAL_CODE)).thenThrow(responseStatusException);

        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> addressService.findOpenCepByCep(POSTAL_CODE));

        assertNotNull(ex);
        assertEquals(responseStatusException.getReason(), ex.getReason());
        assertEquals(responseStatusException.getStatus(), ex.getStatus());

        verify(openCepClient, times(1)).findByCep(POSTAL_CODE);
        verify(cepAddressMapper, times(0)).openCepAddressToCepAddress(any(OpenCepAddressDTO.class));
    }

    @Test
    void findOpenCepByCep_whenMapperReturnException_thenReturnException() {

        OpenCepAddressDTO openCepAddressDTO = OpenCepAddressDTO.builder().postalCode(POSTAL_CODE_OPEN_CEP).build();
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(openCepClient.findByCep(POSTAL_CODE)).thenReturn(openCepAddressDTO);
        when(cepAddressMapper.openCepAddressToCepAddress(openCepAddressDTO)).thenThrow(runtimeException);

        RuntimeException ex = assertThrows(RuntimeException.class, () -> addressService.findOpenCepByCep(POSTAL_CODE));

        assertNotNull(ex);
        assertEquals(runtimeException.getMessage(), ex.getMessage());

        verify(openCepClient, times(1)).findByCep(POSTAL_CODE);
        verify(cepAddressMapper, times(1)).openCepAddressToCepAddress(any(OpenCepAddressDTO.class));
    }

    @Test
    void findOpenCepByCep_whenMapperReturnSuccess_thenReturnSuccess() {

        OpenCepAddressDTO openCepAddressDTO = OpenCepAddressDTO.builder().postalCode(POSTAL_CODE_OPEN_CEP).build();
        CepAddressDTO cepAddressDTO = CepAddressDTO.builder().postalCode(POSTAL_CODE).build();

        when(openCepClient.findByCep(POSTAL_CODE)).thenReturn(openCepAddressDTO);
        when(cepAddressMapper.openCepAddressToCepAddress(openCepAddressDTO)).thenReturn(cepAddressDTO);

        CepAddressDTO openCepByCep = addressService.findOpenCepByCep(POSTAL_CODE);

        assertNotNull(openCepByCep);
        assertEquals(cepAddressDTO, openCepByCep);

        verify(openCepClient, times(1)).findByCep(POSTAL_CODE);
        verify(cepAddressMapper, times(1)).openCepAddressToCepAddress(any(OpenCepAddressDTO.class));
    }

    @Test
    void areAddressesValid_whenAddressesIsNull_thenReturnTrue() {
        boolean areAddressesValid = addressService.areAddressesValid(null);

        assertTrue(areAddressesValid);
    }

    @Test
    void areAddressesValid_whenAddressesIsEmpty_thenReturnTrue() {
        boolean areAddressesValid = addressService.areAddressesValid(List.of());

        assertTrue(areAddressesValid);
    }

    @Test
    void areAddressesValid_whenOneAddressPostalCodeIsNull_thenReturnFalse() {
        List<AddressDTO> addresses = List.of(AddressDTO.builder().build());

        boolean areAddressesValid = addressService.areAddressesValid(addresses);

        assertFalse(areAddressesValid);
    }

    @Test
    void areAddressesValid_whenOneAddressStreetIsDifferentToOpenCepStreet_thenReturnFalse() {
        AddressDTO addressDTO = AddressDTO.builder().postalCode(POSTAL_CODE).build();
        List<AddressDTO> addresses = List.of(addressDTO);
        OpenCepAddressDTO openCepAddressDTO = OpenCepAddressDTO.builder().postalCode(POSTAL_CODE_OPEN_CEP).street("").build();
        CepAddressDTO cepAddressDTO = CepAddressDTO.builder().postalCode(POSTAL_CODE).street("").build();

        when(openCepClient.findByCep(POSTAL_CODE)).thenReturn(openCepAddressDTO);
        when(cepAddressMapper.openCepAddressToCepAddress(openCepAddressDTO)).thenReturn(cepAddressDTO);

        boolean areAddressesValid = addressService.areAddressesValid(addresses);

        assertFalse(areAddressesValid);
    }

    @Test
    void areAddressesValid_whenAddressesAreValid_thenReturnTrue() {
        AddressDTO addressDTO = AddressDTO.builder().postalCode(POSTAL_CODE).build();
        List<AddressDTO> addresses = List.of(addressDTO);
        OpenCepAddressDTO openCepAddressDTO = OpenCepAddressDTO.builder().postalCode(POSTAL_CODE_OPEN_CEP).build();
        CepAddressDTO cepAddressDTO = CepAddressDTO.builder().postalCode(POSTAL_CODE).build();

        when(openCepClient.findByCep(POSTAL_CODE)).thenReturn(openCepAddressDTO);
        when(cepAddressMapper.openCepAddressToCepAddress(openCepAddressDTO)).thenReturn(cepAddressDTO);

        boolean areAddressesValid = addressService.areAddressesValid(addresses);

        assertTrue(areAddressesValid);
    }
}
