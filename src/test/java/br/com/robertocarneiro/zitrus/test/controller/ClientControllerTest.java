package br.com.robertocarneiro.zitrus.test.controller;

import br.com.robertocarneiro.zitrus.test.dto.ClientDTO;
import br.com.robertocarneiro.zitrus.test.service.ClientService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static java.util.Objects.isNull;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.StringUtils.hasText;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ClientController.class)
@WithMockUser
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void findAll_whenReturnException_thenReturnJsonArray() throws Exception {

        long page = 0L;
        long size = 10L;
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        when(clientService.findAll(any(Pageable.class))).thenThrow(responseStatusException);

        mockMvc.perform(get("/api/clients?page=" + page + "&size=" + size)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(responseStatusException.getReason())));

        verify(clientService, times(1)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void findAll_whenReturnClientPage_thenReturnJsonArray() throws Exception {

        int page = 0;
        int size = 10;
        int total = 1;
        Sort sort = Sort.by(Sort.Direction.ASC, "fullName");
        Pageable pageable = PageRequest.of(page, size, sort);

        String pathJson = "static/json/findAllClients_response200.json";
        List<ClientDTO> clients = buildDTOsByJson(pathJson, new TypeReference<>() {});

        if (isNull(clients)) {
            fail("It wasn't possible to read this json file");
        }
        Page<ClientDTO> clientsPage = new PageImpl<>(clients, pageable, total);

        when(clientService.findAll(any(Pageable.class))).thenReturn(clientsPage);

        mockMvc.perform(get("/api/clients?page=" + page + "&size=" + size)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].cpf", is("94475835046")))
                .andExpect(jsonPath("$.content[0].fullName", is("Nome Sobrenome 01")))
                .andExpect(jsonPath("$.content[0].birthDate", is("22/11/1991")))
                .andExpect(jsonPath("$.content[0].addresses", hasSize(1)))
                .andExpect(jsonPath("$.content[0].addresses[0].postalCode", is("12345678")))
                .andExpect(jsonPath("$.content[0].addresses[0].street", is("Rua Amirosvaldo")))
                .andExpect(jsonPath("$.content[0].addresses[0].number", is("1234")))
                .andExpect(jsonPath("$.content[0].addresses[0].complement", is("Apt 2102, bloco 1")))
                .andExpect(jsonPath("$.content[0].addresses[0].district", is("Bairro")))
                .andExpect(jsonPath("$.content[0].addresses[0].city", is("Brasília")))
                .andExpect(jsonPath("$.content[0].addresses[0].state", is("DF")))
                .andExpect(jsonPath("$.pageable.sort.empty", is(false)))
                .andExpect(jsonPath("$.pageable.sort.unsorted", is(false)))
                .andExpect(jsonPath("$.pageable.sort.sorted", is(true)))
                .andExpect(jsonPath("$.pageable.offset", is(0)))
                .andExpect(jsonPath("$.pageable.pageNumber", is(page)))
                .andExpect(jsonPath("$.pageable.pageSize", is(size)))
                .andExpect(jsonPath("$.pageable.unpaged", is(false)))
                .andExpect(jsonPath("$.pageable.paged", is(true)))
                .andExpect(jsonPath("$.last", is(true)))
                .andExpect(jsonPath("$.totalPages", is(1)))
                .andExpect(jsonPath("$.totalElements", is(total)))
                .andExpect(jsonPath("$.size", is(size)))
                .andExpect(jsonPath("$.number", is(page)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.sort.empty", is(false)))
                .andExpect(jsonPath("$.sort.unsorted", is(false)))
                .andExpect(jsonPath("$.sort.sorted", is(true)))
                .andExpect(jsonPath("$.numberOfElements", is(total)))
                .andExpect(jsonPath("$.empty", is(false)));

        verify(clientService, times(1)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void findByCpf_whenReturnException_thenReturnJson() throws Exception {

        String cpf = "12345678901";
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        when(clientService.findByCpf(cpf)).thenThrow(responseStatusException);

        mockMvc.perform(get("/api/clients/" + cpf)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(responseStatusException.getReason())));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(1)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void findByCpf_whenReturnClientDetails_thenReturnJson() throws Exception {

        String pathJson = "static/json/findClientByCpf_response200.json";
        ClientDTO clientDTO = buildDTOsByJson(pathJson, new TypeReference<>() {});
        if (isNull(clientDTO)) {
            fail("It wasn't possible to read this json file");
        }

        when(clientService.findByCpf(clientDTO.getCpf())).thenReturn(clientDTO);

        mockMvc.perform(get("/api/clients/" + clientDTO.getCpf())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cpf", is("94475835046")))
                .andExpect(jsonPath("$.fullName", is("Nome Sobrenome 01")))
                .andExpect(jsonPath("$.birthDate", is("22/11/1991")))
                .andExpect(jsonPath("$.addresses", hasSize(1)))
                .andExpect(jsonPath("$.addresses[0].postalCode", is("12345678")))
                .andExpect(jsonPath("$.addresses[0].street", is("Rua Amirosvaldo")))
                .andExpect(jsonPath("$.addresses[0].number", is("1234")))
                .andExpect(jsonPath("$.addresses[0].complement", is("Apt 2102, bloco 1")))
                .andExpect(jsonPath("$.addresses[0].district", is("Bairro")))
                .andExpect(jsonPath("$.addresses[0].city", is("Brasília")))
                .andExpect(jsonPath("$.addresses[0].state", is("DF")));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(1)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void save_whenReturnException_thenReturnJson() throws Exception {

        String pathJson = "static/json/findClientByCpf_response200.json";
        String bodyContent = writeStringByPath(pathJson);
        if (!hasText(bodyContent)) {
            fail("It wasn't possible to read this json file");
        }
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        when(clientService.create(any(ClientDTO.class))).thenThrow(responseStatusException);

        mockMvc.perform(post("/api/clients")
                .content(bodyContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(responseStatusException.getReason())));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(1)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void save_whenReturnClientDetails_thenReturnJson() throws Exception {

        String pathJson = "static/json/findClientByCpf_response200.json";
        String bodyContent = writeStringByPath(pathJson);
        if (!hasText(bodyContent)) {
            fail("It wasn't possible to read this json file");
        }
        ClientDTO clientDTO = buildDTOsByJson(pathJson, new TypeReference<>() {});
        if (isNull(clientDTO)) {
            fail("It wasn't possible to read this json file");
        }

        when(clientService.create(any(ClientDTO.class))).thenReturn(clientDTO);

        mockMvc.perform(post("/api/clients")
                .content(bodyContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.cpf", is("94475835046")))
                .andExpect(jsonPath("$.fullName", is("Nome Sobrenome 01")))
                .andExpect(jsonPath("$.birthDate", is("22/11/1991")))
                .andExpect(jsonPath("$.addresses", hasSize(1)))
                .andExpect(jsonPath("$.addresses[0].postalCode", is("12345678")))
                .andExpect(jsonPath("$.addresses[0].street", is("Rua Amirosvaldo")))
                .andExpect(jsonPath("$.addresses[0].number", is("1234")))
                .andExpect(jsonPath("$.addresses[0].complement", is("Apt 2102, bloco 1")))
                .andExpect(jsonPath("$.addresses[0].district", is("Bairro")))
                .andExpect(jsonPath("$.addresses[0].city", is("Brasília")))
                .andExpect(jsonPath("$.addresses[0].state", is("DF")));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(1)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void edit_whenReturnException_thenReturnJson() throws Exception {

        String pathJson = "static/json/findClientByCpf_response200.json";
        String bodyContent = writeStringByPath(pathJson);
        if (!hasText(bodyContent)) {
            fail("It wasn't possible to read this json file");
        }
        ClientDTO clientDTO = buildDTOsByJson(pathJson, new TypeReference<>() {});
        if (isNull(clientDTO)) {
            fail("It wasn't possible to read this json file");
        }
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        when(clientService.edit(anyString(), any(ClientDTO.class))).thenThrow(responseStatusException);

        mockMvc.perform(put("/api/clients/" + clientDTO.getCpf())
                .content(bodyContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(responseStatusException.getReason())));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(1)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void edit_whenReturnClientDetails_thenReturnJson() throws Exception {

        String pathJson = "static/json/findClientByCpf_response200.json";
        String bodyContent = writeStringByPath(pathJson);
        if (!hasText(bodyContent)) {
            fail("It wasn't possible to read this json file");
        }
        ClientDTO clientDTO = buildDTOsByJson(pathJson, new TypeReference<>() {});
        if (isNull(clientDTO)) {
            fail("It wasn't possible to read this json file");
        }

        when(clientService.edit(anyString(), any(ClientDTO.class))).thenReturn(clientDTO);

        mockMvc.perform(put("/api/clients/" + clientDTO.getCpf())
                .content(bodyContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cpf", is("94475835046")))
                .andExpect(jsonPath("$.fullName", is("Nome Sobrenome 01")))
                .andExpect(jsonPath("$.birthDate", is("22/11/1991")))
                .andExpect(jsonPath("$.addresses", hasSize(1)))
                .andExpect(jsonPath("$.addresses[0].postalCode", is("12345678")))
                .andExpect(jsonPath("$.addresses[0].street", is("Rua Amirosvaldo")))
                .andExpect(jsonPath("$.addresses[0].number", is("1234")))
                .andExpect(jsonPath("$.addresses[0].complement", is("Apt 2102, bloco 1")))
                .andExpect(jsonPath("$.addresses[0].district", is("Bairro")))
                .andExpect(jsonPath("$.addresses[0].city", is("Brasília")))
                .andExpect(jsonPath("$.addresses[0].state", is("DF")));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(1)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(0)).delete(anyString());
    }

    @Test
    void delete_whenReturnException_thenReturnJson() throws Exception {

        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        doThrow(responseStatusException).when(clientService).delete(anyString());

        mockMvc.perform(delete("/api/clients/12345678901")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(responseStatusException.getReason())));

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(1)).delete(anyString());
    }

    @Test
    void delete_whenReturnClientDetails_thenReturnJson() throws Exception {

        mockMvc.perform(delete("/api/clients/12345678901")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(clientService, times(0)).findAll(any(Pageable.class));
        verify(clientService, times(0)).findByCpf(anyString());
        verify(clientService, times(0)).create(any(ClientDTO.class));
        verify(clientService, times(0)).edit(anyString(), any(ClientDTO.class));
        verify(clientService, times(1)).delete(anyString());
    }

    private <T> T buildDTOsByJson(String path, TypeReference<T> clazz) {
        Resource resource = new ClassPathResource(path);
        try {
            return objectMapper.readValue(resource.getInputStream(), clazz);
        } catch (IOException ex) {
            return null;
        }
    }

    private String writeStringByPath(String path) {
        Resource resource = new ClassPathResource(path);
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            return null;
        }
    }
}
