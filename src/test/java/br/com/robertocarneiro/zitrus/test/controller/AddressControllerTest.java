package br.com.robertocarneiro.zitrus.test.controller;

import br.com.robertocarneiro.zitrus.test.dto.CepAddressDTO;
import br.com.robertocarneiro.zitrus.test.service.AddressService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

import static java.util.Objects.isNull;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AddressController.class)
@WithMockUser
class AddressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AddressService addressService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void findOpenCepByCep_whenReturnResponseStatusException_thenReturnJson() throws Exception {

        String cep = "12345678";
        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Testing");

        when(addressService.findOpenCepByCep(cep)).thenThrow(responseStatusException);

        mockMvc.perform(get("/api/addresses/" + cep + "/open-cep")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(responseStatusException.getReason())));

        verify(addressService, times(1)).findOpenCepByCep(anyString());
    }

    @Test
    void findOpenCepByCep_whenReturnRuntimeException_thenReturnJson() throws Exception {

        String cep = "12345678";
        RuntimeException runtimeException = new RuntimeException("Testing");

        when(addressService.findOpenCepByCep(cep)).thenThrow(runtimeException);

        mockMvc.perform(get("/api/addresses/" + cep + "/open-cep")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.messages", hasSize(1)))
                .andExpect(jsonPath("$.messages[0]", is(runtimeException.getMessage())));

        verify(addressService, times(1)).findOpenCepByCep(anyString());
    }

    @Test
    void findOpenCepByCep_whenReturnClientDetails_thenReturnJson() throws Exception {

        String pathJson = "static/json/findAddressByCep_response200.json";
        CepAddressDTO cepAddressDTO = buildDTOsByJson(pathJson, new TypeReference<>() {});
        if (isNull(cepAddressDTO)) {
            fail("It wasn't possible to read this json file");
        }

        when(addressService.findOpenCepByCep(cepAddressDTO.getPostalCode())).thenReturn(cepAddressDTO);

        mockMvc.perform(get("/api/addresses/" + cepAddressDTO.getPostalCode() + "/open-cep")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postalCode", is("12345678")))
                .andExpect(jsonPath("$.street", is("Rua Amirosvaldo")))
                .andExpect(jsonPath("$.complement", is("de 741/742 a 2029/2030")))
                .andExpect(jsonPath("$.district", is("Bairro")))
                .andExpect(jsonPath("$.city", is("Brasília")))
                .andExpect(jsonPath("$.state", is("DF")));

        verify(addressService, times(1)).findOpenCepByCep(anyString());
    }

    private <T> T buildDTOsByJson(String path, TypeReference<T> clazz) {
        Resource resource = new ClassPathResource(path);
        try {
            return objectMapper.readValue(resource.getInputStream(), clazz);
        } catch (IOException ex) {
            return null;
        }
    }
}
