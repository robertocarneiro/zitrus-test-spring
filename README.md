# Api Client and Address

## Documentation

### API

- Swagger-local: [http://localhost:8090/swagger-ui/index.html](http://localhost:8090/swagger-ui/index.html)

### Maven

To build the application, execute the command below:

```sh
$ mvn clean install
```
## Running

### Maven

To execute the application, execute the command below:

```sh
$ mvn clean package spring-boot:run
```

### Docker

File 'Dockerfile' is on project's root folder

## Build docker image
```
docker build -t robertocsf/zitrus-test-spring .
```

## Push docker image
```
docker push robertocsf/zitrus-test-spring
```

### Docker Compose

File 'docker-compose.yml' is on project's root folder

```
docker-compose up -d
```